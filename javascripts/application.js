var player;
var donne = false;
var tag = document.createElement('script')
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = $('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var selectors = [
  '#vid1',
  '#vid2',
  '#vid3',
  '#redirect1',
  '#redirect2',
  '#redirect3'
];
var youtubeVideoIDs = [
  'c0IykwK6zkY',
  'uCJDLgQ6xFk',
  'HM20wwzl4Ds',
  'uCJDLgQ6xFk',
  'voLy3ukXYXc ',
  'JwpUfcA0ZXk'
];

$(document).ready( function() {
  createCloseButton();
  $('img#circular-close-button').hide();
  $('#comedy').on('click', function(){
    comedyVideos();
  });

  selectors.map(addClickListenersToVideoBoxes);
});

function closeYoutubeVideo() {
  player.destroy();
  $('.navbar').show();
  $('.wrapper').show();
}

function openYoutubePlayer(vidID) {

  function onYouTubeIframeAPIReady(vidID) {
    player = new YT.Player('player', {
      height: '390',
      width: '640',
      videoId: vidID,
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerReady(event) {
    event.target.playVideo();
  }

  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !donne) {
      setTimeout(stopVideo, 6000);
      donne = true;
    }
  }

  function stopVideo() {
    player.stopVideo();
  }

  onYouTubeIframeAPIReady(vidID);
}


function comedyVideos() {

  var watchHereVideos = [
    'Sharknado 3 Funny scenes',
    'Billy Burr: Let it go (2011)',
    'Funny football scenes'
  ];

  var redirectedVideos = [
    'Rick and Morty',
    'Hangover',
    'Bojack Horseman'
  ];

  var vidImages = [
    'images/billy-08.jpg',
    'images/billy-08.jpg',
    'images/billy-08.jpg'
  ];

  var recImages = [
    'images/rickandmorty.jpg',
    'images/hangover-09.jpg',
    'images/bojackhorseman-09.jpg'
  ]

  for(var i = 1; i <= 3; i++){
    watchHereID = '#vid' + i;
    redirectID = '#redirect' + i;
    $(watchHereID).find('.video-title').text(watchHereVideos[i - 1]);
    $(redirectID).find('.video-title').text(redirectedVideos[i - 1]);
    $(watchHereID).find('.img').attr('src', vidImages[i - 1]);
    $(redirectID).find('.img').attr('src', recImages[i - 1]);

  }

}

function createVideoWrapper() {
  $('<div/>', {
    id: 'video-wrapper'
  }).appendTo('body');
}

function createPlayer() {
  $('<div/>', {
    id: 'player'
  }).appendTo('#video-wrapper');
}

function createCloseButton() {
  $('<img/>', {
    id: 'circular-close-button',
    src: 'images/close-button.png'
  }).appendTo('body');
}

function addClickListenersToVideoBoxes(selector) {
  $(selector).on('click', function() {
    createVideoWrapper();
    createPlayer();

    $('img#circular-close-button').on('click', function() {
      $('img#circular-close-button').hide();
      closeYoutubeVideo();
    })
    $('.navbar').hide();
    $('.wrapper').hide();
    $('img#circular-close-button').show();

    var selectorIndex = selectors.indexOf(selector); 
    var videoID = youtubeVideoIDs[selectorIndex]; 
    openYoutubePlayer(videoID);
  });
}
